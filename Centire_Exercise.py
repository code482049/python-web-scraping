﻿from bs4 import BeautifulSoup  # HTML parsing
from urllib.request import urlopen  # Accessing the url
import pandas as pd  # For easy HTML table to data frame conversion
import re  # For searching through the address column
import requests  # For REST API
import json  # For saving the response into a JSON file

# All above mentioned libraries have been updated to the latest version as of 18/04/2024

# Gets the desired HTML table from the website
def get_html_table(url):
    # Accessing the url
    page = urlopen(url)
    
    # Reading and decoding the website
    html = page.read().decode("utf-8")
    
    # Parsing the HTML file
    soup = BeautifulSoup(html, "html.parser")
    
    # Pandas reads the HTML and extracts tables
    tables = pd.read_html(str(soup))
    
    # Loads a single table into data frame and returns it
    return pd.DataFrame(tables[0])

# Extracts and formats the postal code from the address column
def get_postal_codes(address_series):
    # Extract postal codes using regex and remove non-breaking space
    return address_series.str.replace("\xa0", "").str.extract(r'(\d{5})')[0]

# Formats the ID number list
def format_id_num_list(id_num_series):
    # Remove non-breaking space character from ID numbers
    return id_num_series.str.replace("\xa0", "")

# Fetch sales data for a given ID number
def fetch_sales_data(id_num):
    endpoint = f"https://restapi.valida.sk/zadanie/{id_num}"
    headers = {"Authorization": "qcoRJS6F83iPVIF"}
    response = requests.get(endpoint, headers=headers)
    
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Error {response.status_code} for ID {id_num}")
        return None

# Define a custom key function to handle cases where sales are null    
def key_func(item):
    # Negate and treat null values as smallest
    return -(int(item['trzby']) if item and item.get('trzby') else float('-inf'))

# Print postal codes next to their corresponding ID numbers
def print_postal_codes_with_ids(table):
    # Select and print relevant columns
    print(table[['FormattedID', 'PostalCode']])

def main():
    url = "https://nbs.sk/platby/platobne-systemy/sips/zoznam-ucastnikov-platobneho-systemu-sips/"
    
    # Getting the table from HTML file
    table = get_html_table(url)
    
    # Extract and print postal codes
    table['PostalCode'] = get_postal_codes(table['Adresa'])
    print("Postal Codes:")
    print(table['PostalCode'].tolist())
        
    # Format ID numbers (IČO)
    table['FormattedID'] = format_id_num_list(table['IČO'])
    
    # Print postal codes with their corresponding IDs
    print_postal_codes_with_ids(table)
    
    # Fetch and store sales data
    print("\nFetching sales data...")
    sales_data = [fetch_sales_data(id_num) for id_num in table['FormattedID']]
    
    # Sort the sales data
    sorted_data = sorted(filter(None, sales_data), key=key_func)
    
    # Dump the sales data list into the JSON file
    with open("Sales_Data.json", "w", encoding='utf-8') as f:
        json.dump(sorted_data, f, ensure_ascii=False, indent=4)

# Execute the code
if __name__ == '__main__':
    main()